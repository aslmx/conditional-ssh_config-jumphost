# conditional ssh_config jumphost



## What is ssh_config?

`ssh_config` allows you to preconfigure your ssh connections on most *nix machines. You can define hosts and aliases and instead of typing

`ssh user@ssh.example.com -p 1234 -i ~/.ssh/id_rsa -D 7070`

to start a ssh session to host `ssh.example.com` on port 1234 with public key authentication and define a dynmic port forwarding, you can as well put the following in your `~/.ssh/config` file:

```
Host home
    HostName ssh.example.com
    Port 1234
    User user
    IdentityFile ~/.ssh/id_rsa
    DynamicForward  7070
```

And then start your ssh connection with just

`ssh home`


## What is a jumphost?

Imagine you have a setup like the follwing

![](res/jump.drawio.png)

You can reach the host `jumphost` directly from the internet, but you can't reach host `internal`.

To ssh into `internal` you can first ssh into `jumphost` and from there you can ssh into `internal`. Or you can use the `ProxyJump` feature:

```
Host jumphost
    HostName ssh.example.com
    Port 1234
    User user
    IdentityFile ~/.ssh/id_rsa
    DynamicForward  7070

Host internal
    HostName 192.168.2.123
    Port 22
    User user
    ProxyJump jumphost
```

If you now type

`ssh internal` 

ssh will take care and use `jumphost` as a intermediate connection when connecting to `internal`

## I don't want to use the Jump Host all the time?

Imagine, sometimes you are at home, sometimes you are not. You can solve this by using two Hosts for `internal` like this:

```
Host jumphost
    HostName ssh.example.com
    Port 1234
    User user
    IdentityFile ~/.ssh/id_rsa
    DynamicForward  7070

Host internal-external
    HostName 192.168.2.123
    Port 22
    User user
    ProxyJump jumphost

Host internal
    HostName 192.168.2.123
    Port 22
    User user

```

Which kind of sucks ;) 

But you can use the `exec` feature of ssh. This will exec a little script and based on the outcome, it will do this or that...

```
Host jumphost
    HostName ssh.example.com
    Port 1234
    User user
    IdentityFile ~/.ssh/id_rsa
    DynamicForward  7070

Match host internal !exec "/home/user/tools/check_home.sh" 
    ProxyJump jumphost
Host internal
    HostName 192.168.2.123
    Port 22
    User user
```


If you now type

`ssh internal`, ssh will execution `check_home.sh` and based on its exit code will use the proxy jump feature or not. 


## check_home.sh

This is just a tiny little script that will

1. ping a known host in your network, e.g. your router
2. check the arp command for a known MAC address


```
#!/bin/bash


ping 192.168.2.1 -c 1 -W 0,01 2>&1 > /dev/null && arp -a | grep "(192.168.2.1)" | cut -d " " -f 4 | grep "aa:bb:cc:dd:ee:ff"

```


Make sure to change `aa:bb:cc:dd:ee:ff` and `192.168.2.1` into the MAC and IP address of your router.



## How does this work? 

`man 5 ssh_config` tells us

> Match   Restricts the following declarations (up to the next Host or Match keyword) to be used only when the conditions following the Match keyword are satisfied.

and

> The exec keyword executes the specified command under the user's shell.  If the command returns a zero exit status then the condition is considered true.

The command in check_home.sh will return 0 if it can grep your routers MAC address. If it can't, it will return 1.  The `!` in front of the `exec` will take care of matching in this case.


